/*
	COMPRESS_TURBOPFOR.H
	---------------------
*/
#ifndef COMPRESS_TURBOPFOR_H_
#define COMPRESS_TURBOPFOR_H_

#include <stdint.h>
#include "compress_turbopfor_internals.h"

/*
	class ANT_COMPRESS_TURBOPFOR
	----------------------------
*/
class ANT_compress_turbopfor : public ANT_compress
{
public:
	ANT_compress_turbopfor() {}
	virtual ~ANT_compress_turbopfor() {}

	virtual uint64_t compress(uint8_t *destination, uint64_t destination_length, uint32_t *source, uint64_t source_integers)
		{
		return turbopfor_compress(destination, destination_length, source, source_integers);
		}

	virtual void decompress(uint32_t *destination, uint64_t destinaton_integers, uint8_t *source, uint64_t source_length)
		{
		turbopfor_decompress(destination, destinaton_integers, source, source_length);
		}
} ;

#endif

