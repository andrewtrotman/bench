/*
	TIMER.H
	-------
*/
#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include <chrono>

typedef std::chrono::time_point<std::chrono::steady_clock> timer;
typedef std::chrono::nanoseconds timer_duration;

timer timer_start()
	{
	return std::chrono::steady_clock::now();
	}

timer_duration timer_stop(timer then)
	{
	return std::chrono::steady_clock::now() - then;
	}

int64_t timer_ms(timer_duration duration)
	{
	return std::chrono::duration <double, std::milli> (duration).count();
	}

int64_t timer_ns(timer_duration duration)
	{
	return std::chrono::duration <double, std::nano> (duration).count();
	}

#endif