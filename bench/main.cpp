/*
	BENCH.CPP
	---------
	Run through the Lemire gov2 collection and compress, decompress (and verify it worked) each postings list.  The format of that file is:
		length (4-byte integer)
		posting (length * 4-byte integers)
	repreated until end of file
*/
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "compress_qmx_adcs.h"
#include "compress_qmx.h"
#include "compress_qmx_v2.h"
#include "compress_qmx_v3.h"
#include "compress_qmx_v4.h"
#include "compress_opt.h"
#include "compress_turbopackv.h"
#include "compress_turbopfor.h"
#include "timer.h"

#define NUMBER_OF_DOCUMENTS (1024 * 1024 * 128)

/*
	Buffer to hold the contents of the current postings list.  This is way too large, but only allocated once.
*/
static uint32_t *postings_list;

/*
	Buffer to hold the compressed postings list
*/
static uint32_t *compressed_postings_list;

/*
	Buffer to hold the decompressed postings list - after compression then decompression this should equal postings_list[]
*/
static uint32_t *decompressed_postings_list;

/*
	Buffer holding the sum of times to decompress postings lists of this length
*/
static uint32_t *decompress_time;

/*
	Buffer holding the number of postings list of this length
*/
static uint32_t *decompress_count;

/*
	Buffer holding the size (in bytes) of each compressed string
*/
static uint32_t *compressed_size;

/*
	Buffer holding the number of postings list of this length
*/
static uint32_t *compressed_count;		// should be identical to decompress_count


/*
	The compressors we're going to use
*/
ANT_compress *QMX_adcs_shrinkerator = new ANT_compress_qmx_adcs;
ANT_compress *QMX_shrinkerator = new ANT_compress_qmx;
ANT_compress *QMXV2_shrinkerator = new ANT_compress_qmx_v2;
ANT_compress *QMXV3_shrinkerator = new ANT_compress_qmx_v3;
ANT_compress *QMXV4_shrinkerator = new ANT_compress_qmx_v4;
ANT_compress *OPT_shrinkerator = new ANT_compress_opt;
ANT_compress *TurboPackV_shrinkerator = new ANT_compress_turbopackv;
ANT_compress *TurboPfor_shrinkerator = new ANT_compress_turbopfor;

/*
	DRAW_HISTOGRAM()
	----------------
*/
void draw_histogram(void)
{
printf("length DecompressTimeInNanoseconds CompressedSizeInBytes\n");
for (uint32_t index = 0; index < NUMBER_OF_DOCUMENTS; index++)
	if (decompress_time[index] != 0)
		printf("%u %u %u\n", index, decompress_time[index] / decompress_count[index], compressed_size[index] / compressed_count[index]);
}

/*
	GENERATE_DIFFERENCES()
	----------------------
*/
void generate_differences(uint32_t *postings_list, uint32_t length)
{
uint32_t previous = 0;

for (uint32_t current = 0; current < length; current++)
	{
	uint32_t was;
	was = postings_list[current];

	postings_list[current] -= previous;
	previous = was;
	}
}

/*
	USAGE()
	-------
*/
void usage(const char *exename)
{
exit(printf("Usage:%s [<QMX>|<QMXv2>|<QMXv3>|<QMXv4>|<QMXADCS>|<OPT>|<TurboPackV>|<TurboPfor>] <infile>\n", exename));
}

/*
	MAIN()
	------
*/
int main(int argc, char *argv[])
{
const char *filename;
ANT_compress *shrinkerator = OPT_shrinkerator;

/*
	Check parameters
*/
if (argc <= 1)
	usage(argv[0]);	// filename = "/Volumes/Other/data/gov2.sorted";			// debug from the IDE
else if (argc == 2)
	if (argv[1][0] == '-')
		usage(argv[0]);												// first parameter is a '-' to give help
	else
		filename = argv[1];											// else the first parameter is the filename to use
else if (argc == 3)
	{
	if (strcmp(argv[1], "QMXADCS") == 0)
		shrinkerator = QMX_adcs_shrinkerator;
	else if (strcmp(argv[1], "QMXv4") == 0)
		shrinkerator = QMXV4_shrinkerator;
	else if (strcmp(argv[1], "QMXv3") == 0)
		shrinkerator = QMXV3_shrinkerator;
	else if (strcmp(argv[1], "QMXv2") == 0)
		shrinkerator = QMXV2_shrinkerator;
	else if (strcmp(argv[1], "QMX") == 0)
		shrinkerator = QMX_shrinkerator;
	else if (strcmp(argv[1], "OPT") == 0)
		shrinkerator = OPT_shrinkerator;
	else if (strcmp(argv[1], "TurboPackV") == 0)
		shrinkerator = TurboPackV_shrinkerator;
	else if (strcmp(argv[1], "TurboPfor") == 0)
		shrinkerator = TurboPfor_shrinkerator;
	else
		{
		puts("UNKNOWN COMPRESSION SCHEME");
		exit(0);
		}
	filename = argv[2];
	}
else
	usage(argv[0]);

if (shrinkerator == QMX_adcs_shrinkerator)
	puts("Scheme:QMX ADCS");
else if (shrinkerator == QMXV4_shrinkerator)
	puts("Scheme:QMXv4");
else if (shrinkerator == QMXV3_shrinkerator)
	puts("Scheme:QMXv3");
else if (shrinkerator == QMXV2_shrinkerator)
	puts("Scheme:QMXv2");
else if (shrinkerator == QMX_shrinkerator)
	puts("Scheme:QMX");
else if (shrinkerator == OPT_shrinkerator)
	puts("Scheme:OPT");
else if (shrinkerator == TurboPackV_shrinkerator)
	puts("Scheme:TurboPackV");
else if (shrinkerator == TurboPfor_shrinkerator)
	puts("Scheme:TurboPfor");
else
	{
	puts("Unknown compression scheme");
	exit(0);
	}

/*
	Initialise by setting the count buffers to 0
*/
postings_list = new uint32_t[NUMBER_OF_DOCUMENTS];
compressed_postings_list = new uint32_t[NUMBER_OF_DOCUMENTS];
decompressed_postings_list = new uint32_t[NUMBER_OF_DOCUMENTS];
decompress_time = new uint32_t[NUMBER_OF_DOCUMENTS];
decompress_count = new uint32_t[NUMBER_OF_DOCUMENTS];
compressed_size = new uint32_t[NUMBER_OF_DOCUMENTS];
compressed_count = new uint32_t[NUMBER_OF_DOCUMENTS];

memset(decompress_time, 0, NUMBER_OF_DOCUMENTS  * sizeof(*decompress_time));
memset(decompress_count, 0, NUMBER_OF_DOCUMENTS  * sizeof(*decompress_count));
memset(compressed_size, 0, NUMBER_OF_DOCUMENTS  * sizeof(*compressed_size));
memset(compressed_count, 0, NUMBER_OF_DOCUMENTS  * sizeof(*compressed_count));

/*
	Open the postings list file
*/
printf("Using:%s\n", filename);
FILE *fp;
if ((fp = fopen(filename, "rb")) == NULL)
	exit(printf("cannot open %s\n", filename));

/*
	Iterate through each postings list in the file
*/
uint32_t length;
uint32_t term_count = 0;
while (fread(&length, sizeof(length), 1, fp)  == 1)
	{
	term_count++;
	/*
		Read one postings list (and make sure we did so successfully)
	*/
	if (fread(postings_list, sizeof(*postings_list), length, fp) != length)
		exit(printf("i/o error\n"));

//printf("Length:%u\n", (unsigned)length);
//fflush(stdout);

	/*
		convert into d1-gaps
	*/
	generate_differences(postings_list, length);

	/*
		Compress
	*/
	uint64_t size_in_bytes_once_compressed;
	size_in_bytes_once_compressed = shrinkerator->compress((uint8_t *)compressed_postings_list, NUMBER_OF_DOCUMENTS * sizeof(*compressed_postings_list), postings_list, length);

	compressed_size[length] += size_in_bytes_once_compressed;
	compressed_count[length]++;

	/*
		Decompress
	*/
	auto timer = timer_start();
	shrinkerator->decompress(decompressed_postings_list, length, (uint8_t *)compressed_postings_list, size_in_bytes_once_compressed);
	auto took = timer_ns(timer_stop(timer));

	decompress_time[length] += took;
	decompress_count[length]++;

//printf("%u integers -> %uns\n", (unsigned)length, (unsigned)took);
//fflush(stdout);

	/*
		Verify
	*/
	if (memcmp(postings_list, decompressed_postings_list, length * sizeof(*postings_list)) != 0)
		{
		printf("Fail on list %u\n", term_count);
		for (uint32_t pos = 0; pos < length; pos++)
			if (postings_list[pos] != decompressed_postings_list[pos])
				printf("Fail at pos:%d\n", (int)pos);
		exit(0);
		}
	/*
		Notify
	*/
#ifdef NEVER
	if (term_count % 1000 == 0)
		{
		printf("Terms:%u\n", (unsigned)term_count);
		fflush(stdout);
		}
#endif

	}

printf("Total terms:%u\n", term_count);
draw_histogram();

return 0;
}
