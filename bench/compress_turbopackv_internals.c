/*
	COMPRESS_TURBOPACKV_INTERNALS.C
	-------------------------------
*/
#include <stdint.h>
#include "conf.h"
#include "bitutil.h"
#include "bitpackv32_.h"
#include "bitpack.h"
#include "bitunpack.h"

/*
	TURBOPACKV_COMPRESS()
	---------------------
*/
uint64_t turbopackv_compress(uint8_t *destination, uint64_t destination_length, uint32_t *source, uint64_t source_integers)
{
int b;
uint32_t *in;
size_t n;
uint8_t *start_pointer = destination;

for (in = source, n = source_integers; n >= 128; in += 128, n -= 128)
	{
	BITSIZE32(in, 128, b);
	*destination++ = b;
	destination = bitpackv32(in, 128, destination, b);
	}

BITSIZE32(in, n, b);
*destination++ = b;
uint8_t *end_pointer = n == 0 ? destination : bitpack32(in, n, destination, b);

return end_pointer - start_pointer;
}

/*
	TURBOPACKV_DECOMPRESS()
	-----------------------
*/
void turbopackv_decompress(uint32_t *destination, uint64_t destination_integers, uint8_t *source, uint64_t source_length)
{
int bits;

while (destination_integers >= 128)
	{
	bits = *source++;
	source = bitunpackv32(source, 128, destination, bits);
	destination_integers -= 128;
	destination += 128;
	}

if (destination_integers != 0)
	{
	bits = *source++;
	bitunpack32(source, destination_integers, destination, bits);
	}
}
