#!/bin/bash

for i in `seq 1 25`
do
	echo "QMXADCS sorted" $i
	./bench QMXADCS gov2.sorted > QMXADCS.sorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX sorted" $i
	./bench QMX gov2.sorted > QMX.sorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX v2 sorted" $i
	./bench QMXv2 gov2.sorted > QMXv2.sorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX v3 sorted" $i
	./bench QMXv3 gov2.sorted > QMXv3.sorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX v4 sorted" $i
	./bench QMXv4 gov2.sorted > QMXv4.sorted.out.$i
done

for i in `seq 1 25`
do
	echo "OPT sorted" $i
	./bench OPT gov2.sorted > OPT.sorted.out.$i
done

for i in `seq 1 25`
do
	echo "TurboPackV sorted" $i
	./bench TurboPackV gov2.sorted > TurboPackV.sorted.out.$i
done

for i in `seq 1 25`
do
	echo "TurboPfor sorted" $i
	./bench TurboPfor gov2.sorted > TurboPfor.sorted.out.$i
done



for i in `seq 1 25`
do
	echo "QMXADCS unsorted" $i
	./bench QMXADCS gov2.unsorted > QMXADCS.unsorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX unsorted" $i
	./bench QMX gov2.unsorted > QMX.unsorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX v2 unsorted" $i
	./bench QMXv2 gov2.unsorted > QMXv2.unsorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX v3 unsorted" $i
	./bench QMXv3 gov2.unsorted > QMXv3.unsorted.out.$i
done

for i in `seq 1 25`
do
	echo "QMX v4 unsorted" $i
	./bench QMXv4 gov2.unsorted > QMXv4.unsorted.out.$i
done

for i in `seq 1 25`
do
	echo "OPT unsorted" $i
	./bench OPT gov2.unsorted > OPT.unsorted.out.$i
done

for i in `seq 1 25`
do
	echo "TurboPackV unsorted" $i
	./bench TurboPackV gov2.unsorted > TurboPackV.unsorted.out.$i
done

for i in `seq 1 25`
do
	echo "TurboPfor unsorted" $i
	./bench TurboPfor gov2.unsorted > TurboPfor.unsorted.out.$i
done

./process_runs QMXADCS.sorted.out.* > QMXADCS.sorted.out
./process_runs QMX.sorted.out.* > QMX.sorted.out
./process_runs QMXv2.sorted.out.* > QMXv2.sorted.out
./process_runs QMXv3.sorted.out.* > QMXv3.sorted.out
./process_runs QMXv4.sorted.out.* > QMXv4.sorted.out
./process_runs OPT.sorted.out.* > OPT.sorted.out
./process_runs TurboPackV.sorted.out.* > TurboPackV.sorted.out
./process_runs TurboPfor.sorted.out.* > TurboPfor.sorted.out
./process_runs QMXADCS.unsorted.out.* > QMXADCS.unsorted.out
./process_runs QMX.unsorted.out.* > QMX.unsorted.out
./process_runs QMXv2.unsorted.out.* > QMXv2.unsorted.out
./process_runs QMXv3.unsorted.out.* > QMXv3.unsorted.out
./process_runs QMXv4.unsorted.out.* > QMXv4.unsorted.out
./process_runs OPT.unsorted.out.* > OPT.unsorted.out
./process_runs TurboPackV.unsorted.out.* > TurboPackV.unsorted.out
./process_runs TurboPfor.unsorted.out.* > TurboPfor.unsorted.out

./process_processed_runs QMXADCS.unsorted.out QMX.unsorted.out QMXv2.unsorted.out QMXv3.unsorted.out QMXv4.unsorted.out OPT.unsorted.out TurboPackV.unsorted.out TurboPfor.unsorted.out QMXADCS.sorted.out QMX.sorted.out QMXv2.sorted.out QMXv3.sorted.out QMXv4.sorted.out OPT.sorted.out TurboPackV.sorted.out TurboPfor.sorted.out  > summary.txt 
