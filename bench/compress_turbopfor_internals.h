/*
	COMPRESS_TURBOPFOR_INTERNALS.H
	------------------------------
*/
#ifndef COMPRESS_TURBOPFOR_INTERNALS_H_
#define COMPRESS_TURBOPFOR_INTERNALS_H_

#include <stdint.h>

extern "C" uint64_t turbopfor_compress(uint8_t *destination, uint64_t destination_length, uint32_t *source, uint64_t source_integers);
extern "C" void turbopfor_decompress(uint32_t *destination, uint64_t destinaton_integers, uint8_t *source, uint64_t source_length);

#endif

