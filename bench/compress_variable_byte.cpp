/*
	COMPRESS_VARIABLE_BYTE.CPP
	--------------------------
*/
#include "compress_variable_byte.h"

/*
	ANT_COMPRESS_VARIABLE_BYTE::COMPRESS()
	--------------------------------------
*/
uint64_t ANT_compress_variable_byte::compress(uint8_t *destination, uint64_t destination_length, uint32_t *source, uint64_t source_integers)
	{
	uint32_t *current, *end;
	uint32_t needed;
	uint64_t used = 0;

	end = source + source_integers;
	for (current = source; current < end; current++)
		{
		needed = compress_bytes_needed(*current);
		if (used + needed > destination_length)
			return 0;
		compress_into(destination + used, *current);
		used += needed;
		}

	return used;
	}

/*
	ANT_COMPRESS_VARIABLE_BYTE::DECOMPRESS()
	----------------------------------------
*/
void ANT_compress_variable_byte::decompress(uint32_t *destination, uint64_t destination_integers, uint8_t *source, uint64_t source_length)
	{
	uint32_t *end;

	end = destination + destination_integers;

	while (destination < end)
		if (*source & 0x80)
			*destination++ = *source++ & 0x7F;
		else
			{
			*destination = *source++;
			while (!(*source & 0x80))
				*destination = (*destination << 7) | *source++;
			*destination = (*destination << 7) | (*source++ & 0x7F);
			destination++;
			}
	}
