/*
	COMPRESS_VARIABLE_BYTE.H
	------------------------
*/
#ifndef COMPRESS_VARIABLE_BYTE_H_
#define COMPRESS_VARIABLE_BYTE_H_

#include "compress.h"

/*
	class ANT_COMPRESS_VARIABLE_BYTE
	--------------------------------
*/
class ANT_compress_variable_byte : public ANT_compress
	{
	public:
		ANT_compress_variable_byte() {}
		virtual ~ANT_compress_variable_byte() {}

		virtual uint64_t compress(uint8_t *destination, uint64_t destination_length, uint32_t *source, uint64_t source_integers);
		virtual void decompress(uint32_t *destination, uint64_t destination_integers, uint8_t *source, uint64_t source_length);

		static inline uint32_t compress_bytes_needed(uint32_t docno);
		static inline void compress_into(uint8_t *dest, uint32_t docno);
	} ;

/*
	ANT_COMPRESS_VARIABLE_BYTE::COMPRESS_BYTES_NEEDED()
	---------------------------------------------------
*/
inline uint32_t ANT_compress_variable_byte::compress_bytes_needed(uint32_t docno)
	{
	if (docno < ((uint32_t)1 << 7))
		return 1;
	else if (docno < ((uint32_t)1 << 14))
		return 2;
	else if (docno < ((uint32_t)1 << 21))
		return 3;
	else  if (docno < ((uint32_t)1 << 28))
		return 4;
	else
		return 5;
	}

/*
	ANT_COMPRESS_VARIABLE_BYTE::COMPRESS_INTO()
	-------------------------------------------
*/
inline void ANT_compress_variable_byte::compress_into(uint8_t *dest, uint32_t docno)
	{
	if (docno < ((uint32_t)1 << 7))
		goto one;
	else if (docno < ((uint32_t)1 << 14))
		goto two;
	else if (docno < ((uint32_t)1 << 21))
		goto three;
	else if (docno < ((uint32_t)1 << 28))
		goto four;
	else
		goto five;

	five:
		*dest++ = (docno >> 28) & 0x7F;
	four:
		*dest++ = (docno >> 21) & 0x7F;
	three:
		*dest++ = (docno >> 14) & 0x7F;
	two:
		*dest++ = (docno >> 7) & 0x7F;
	one:
		*dest++ = (docno & 0x7F) | 0x80;
	}

#endif
