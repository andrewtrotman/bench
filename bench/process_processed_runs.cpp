/*
	PROCESS_PROCESSED_RUNS.CPP
	--------------------------
	Take the output of process_runs and turn them into a table suitable for loading into excel.
*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <new>
#include <map>
#include <vector>
#include <string>
#include <algorithm>


/*
	CLASS PAIR
	----------
*/
class pair
{
public:
	int size;
	int median;
public:
	pair(int s, int m) : size(s), median(m) {}
};

/*
	GLOBALS
	-------
*/
std::map<int, std::vector<pair>> table;
std::vector<std::string> filenames;

/*
	READ_ENTIRE_FILE()
	------------------
*/
char *read_entire_file(char *filename)
{
char *block = NULL;
FILE *fp;
struct stat details;

if (filename == NULL)
	return NULL;

if ((fp = fopen(filename, "rb")) == NULL)
	return NULL;

if (fstat(fileno(fp), &details) == 0)
	if (details.st_size != 0)
		if ((block = new (std::nothrow) char [(size_t)(details.st_size + 1)]) != NULL)		// +1 for the '\0' on the end
			{
			if (fread(block, details.st_size, 1, fp) != 0)
				block[details.st_size] = '\0';
			else
				{
				delete [] block;
				block = NULL;
				}
			}
fclose(fp);

return block;
}

/*
	BUFFER_TO_LIST()
	----------------
*/
char **buffer_to_list(char *buffer, long long *lines)
{
char *pos, **line_list, **current_line;
long n_frequency, r_frequency;

n_frequency = r_frequency = 0;
for (pos = buffer; *pos != '\0'; pos++)
	if (*pos == '\n')
		n_frequency++;
	else if (*pos == '\r')
		r_frequency++;

*lines = r_frequency > n_frequency ? r_frequency : n_frequency;
current_line = line_list = new (std::nothrow) char * [(size_t)(*lines + 2)]; 		// +1 in case the last line has no \n; +1 for a NULL at the end of the list

if (line_list == NULL)		// out of memory!
	return NULL;

*current_line++ = pos = buffer;
while (*pos != '\0')
	{
	if (*pos == '\n' || *pos == '\r')
		{
		*pos++ = '\0';
		while (*pos == '\n' || *pos == '\r')
			pos++;
		*current_line++ = pos;
		}
	else
		pos++;
	}
/*
	We have a nasty case here.  If the last line has no CR/LF then we need to include it
	but shove a NULL on the next line, but if the last line has a CR/LF then we need to avoid
	adding a blank line to the end of the list.
	NOTE: its 2012 and its a bit late to be finding this bug!!!
*/
if (**(current_line - 1) == '\0')
	*(current_line - 1) = NULL;
*current_line = NULL;

*lines = current_line - line_list - 1;		// the true number of lines

return line_list;
}

/*
	MAIN()
	------
*/
int main(int argc, char *argv[])
{
for (int parameter = 1; parameter < argc; parameter++)
	{
	filenames.push_back(argv[parameter]);
	char *file = read_entire_file(argv[parameter]);
	long long number_of_lines;
	char **lines = buffer_to_list(file, &number_of_lines);

	for (char **line = lines; *line != NULL; line++)
		{
		if (!isdigit(**line))
			continue;

		int integers;
		int size_in_bytes;
		int median;

		/*
			First parameters is the number of integers in the list
			Second parameter is the length of the compressed list (in bytes)
		*/
		sscanf(*line, "%d %d", &integers, &size_in_bytes);

		/*
			The last parameter is the median
		*/
		char *space = strrchr(*line, ' ');
		sscanf(space, "%d", &median);

		/*
			Add them to the lists
		*/
		table[integers].push_back(pair(size_in_bytes, median));
		}

	delete [] lines;
	delete [] file;
	}

printf("integers");
for (const auto &name : filenames)
	printf(" %s.bytes ", name.c_str());
for (const auto &name : filenames)
	printf(" %s.nanoseconds ", name.c_str());
puts("");

for (const auto &line : table)
	{
	printf("%d", line.first);
	for (const auto instance : line.second)
		printf(" %d", instance.size);
	for (const auto instance : line.second)
		printf(" %d", instance.median);
	puts("");
	}

return 0;
}

