/*
	COMPRESS_TURBOPFOR_INTERNALS.C
	------------------------------
*/
#include <stdint.h>
#include "conf.h"
#include "bitutil.h"
#include "bitpackv32_.h"
#include "bitpack.h"
#include "bitunpack.h"
#include "vp4dc.h"
#include "vp4dd.h"

/*
	TURBOPFOR_COMPRESS()
	--------------------
*/
uint64_t turbopfor_compress(uint8_t *destination, uint64_t destination_length, uint32_t *source, uint64_t source_integers)
{
int b;
uint32_t *in;
size_t n;
uint8_t *start_pointer = destination;

for (in = source, n = source_integers; n >= 128; in += 128, n -= 128)
	{
	destination = p4dencv32(in, 128, destination);
	}

uint8_t *end_pointer = n == 0 ? destination : p4denc32(in, n, destination);

return end_pointer - start_pointer;
}

/*
	TURBOPFOR_DECOMPRESS()
	----------------------
	return n == 128?p4ddecv32(in, n, out):p4ddec32(in, n, out);
*/
void turbopfor_decompress(uint32_t *destination, uint64_t destination_integers, uint8_t *source, uint64_t source_length)
{
int bits;

while (destination_integers >= 128)
	{
	source = p4ddecv32(source, 128, destination);
	destination_integers -= 128;
	destination += 128;
	}

if (destination_integers != 0)
	p4ddec32(source, destination_integers, destination);
}
